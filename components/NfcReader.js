import { Button, Text, View } from "react-native"
import React from "react"
import NfcManager, { NfcEvents, NfcTech, Ndef } from 'react-native-nfc-manager';

// Pre-step, call this before any NFC operations
async function initNfc() {
    await NfcManager.start();
}

async function supported() {
    const supported = await NfcManager.isSupported();
    if (supported) {
        await NfcManager.start();
        console.log('supported')
    }
    return supported;
}



export default class NfcReader extends React.Component {
    constructor() {
        super();
        this.state = {
            showNfcPrompt: false,
            storageCache: [],
        };
    }

    state = {
        chipStatus: "",
    }

    componentDidMount() {

        // NfcManager.isSupported().then(r => {
        //     NfcManager.start();
        //     console.log('supported')
        // }).catch(err => {
        //     console.log("not supported " + err);
        // });
    }
    cleanUp = () => {
        console.log('cleanUp')
        NfcManager.setEventListener(NfcEvents.DiscoverTag, null);
        NfcManager.setEventListener(NfcEvents.SessionClosed, null);
    };

    readNdef() {
        NfcManager.isSupported().then(r=>{
            NfcManager.start();
            console.log('supported')
        }).catch(err => {
            console.log("not supported "+err);
        });

        console.log('readNdef')
        return new Promise((resolve) => {
            console.log('try to read')
            let tagFound = null;
            NfcManager.setEventListener(NfcEvents.DiscoverTag, (tag) => {
                tagFound = tag;
                resolve(tagFound);
                var pl = tag.ndefMessage[0].payload
                console.log('NDEF tag found ' + JSON.stringify(tag, null, 2))
                console.log('NFC MSG: ' + JSON.parse(String.fromCharCode(...pl.slice(3,pl.ligth))))
                
                // NfcManager.setAlertMessageIOS('NDEF tag found');
                NfcManager.unregisterTagEvent().catch(() => 0);
            });

            NfcManager.setEventListener(NfcEvents.SessionClosed, () => {
                cleanUp();
                if (!tagFound) {
                    resolve();
                }
            });

            NfcManager.registerTagEvent();
        });
    }

    writeNdef() {
        NfcManager.isSupported().then(r=>{
            NfcManager.start();
            console.log('supported')
        }).catch(err => {
            console.log("not supported "+err);
        });
        console.log('writeNdef')
        // Step 1
        NfcManager.requestTechnology(NfcTech.Ndef, {
            alertMessage: 'Ready to write some NDEF',
        }).then(() => {
            const bytes = Ndef.encodeMessage([Ndef.textRecord('Hello NFC')]);//
            // const bytes = Ndef.encodeMessage([Ndef.textRecord(JSON.stringify({id:123,name:"Ivan"}))]);//'Hello NFC'

            if (bytes) {
                NfcManager.ndefHandler // Step2
                    .writeNdefMessage(bytes).then(r => {
                        console.log('[NFC Write] [INFO] Success write NDEF');
                    }); // Step3
            }
        }).then(() => {
            // Step 4
            NfcManager.cancelTechnologyRequest().catch(() => 0);
        });

    }
    render() {
        return (
            <View>
                <Text>NFC READER Component</Text>
                <Text>Mark ID</Text>
                <Text>Mark data</Text>
                <Button
                    title="read mark"
                    onPress={() => { this.readNdef() }}
                ></Button>
                <Button
                    title="Write mark"
                    onPress={() => { this.writeNdef() }}
                ></Button>
            </View>
        )
    }
}