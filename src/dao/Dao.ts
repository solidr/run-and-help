export default abstract class Dao<T>{
    abstract create(o: T):T;
    // get: (id: number) => T;
    // update: () => T;
    // getAll: () => Array<T>;
    // delete: () => Boolean;
}
