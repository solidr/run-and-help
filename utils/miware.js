import NfcManager, { NfcTech } from 'react-native-nfc-manager';

export default class Miware {

    // Pre-step, call this before any NFC operations
    constructor() {
        await NfcManager.start();
    }

    readMifare() {
        try {
            // 0. Request Mifare technology
            let reqMifare = await NfcManager.requestTechnology(
                NfcTech.MifareUltralight,
            );
            if (reqMifare !== 'MifareUltralight') {
                throw new Error(
                    '[NFC Read] [ERR] Mifare technology could not be requested',
                );
            }

            // 1. Get NFC Tag information
            const nfcTag = await NfcManager.getTag();
            console.log('[NFC Read] [INFO] Tag: ', nfcTag);

            // 2. Read pages
            const readLength = 60;
            let mifarePages = [];
            const mifarePagesRead = await Promise.all(
                [...Array(readLength).keys()].map(async (_, i) => {
                    const pageOffset = i * 4; // 4 Pages are read at once, so offset should be in steps with length 4
                    let pages = await NfcManager.mifareUltralightHandlerAndroid.mifareUltralightReadPages(
                        pageOffset,
                    );
                    mifarePages.push(pages);
                    console.log(`[NFC Read] [INFO] Mifare Page: ${pageOffset}`, pages);
                    //await wait(500); // If Mifare Chip is to slow
                }),
            );

            // 3. Success
            console.log('[NFC Read] [INFO] Success reading Mifare');

            // 4. Cleanup
            _cleanup();
        } catch (ex) {
            console.warn('[NFC Read] [ERR] Failed Reading Mifare: ', ex);
            _cleanup();
        }
    }

    _cleanup() {
        NfcManager.cancelTechnologyRequest().catch(() => 0);
    }

}